const { nanoid }  = require('nanoid')
const querystring = require('querystring')
const SpotifyWebApi = require('spotify-web-api-node');
const config =require('../config');


const credentials = {
    clientId: config.server.clientId,
    clientSecret: config.server.clientSecret,
    redirectUri: `${config.server.baseUrl}/callback`
};

const scopes = ['user-read-private', 'user-read-email'];

const SpotifyHandler = {
    login: async (req, res, next) => {
        
        let state = nanoid(10)
        req.session.state = state
        
        let spotifyApi = new SpotifyWebApi({
            clientId: credentials.clientId,
            redirectUri: credentials.redirectUri
        })
        
        try{     
            let authorizeURL = await spotifyApi.createAuthorizeURL(scopes, state);
            res.redirect(authorizeURL);
            next()
        } catch (err) {
            res.status(500).json({ err })
            next()
        }
    },
    callback: async (req, res, next) => { 
        let code = req.query.code || null;
        let state = req.query.state || null;
        let storedState = req.session.state || null;
        let spotifyApi = new SpotifyWebApi(credentials);

        if (state === null || state !== storedState) {
            res.status(500).json({ error: 'state mismatch'})   
            next()
        }
        else {
            req.session.state = null;
            try { 
                let { body, headers, statusCode } = await spotifyApi.authorizationCodeGrant(code)
                res.status(200).json({ body })
                next();
            }
            catch { next() }
            
        }
    },
    refresh_token: async (req, res, next) => {
        let reftoken = req.query.token || null 
        
        if(reftoken){
            let spotifyApi = new SpotifyWebApi({
                clientId: credentials.clientId,
                clientSecret: credentials.clientSecret,
                refreshToken: refToken
            })
            try {
                let { body, headers, statusCode } = await spotifyApi.refreshAccessToken()
                if(statusCode !== 200) next()
                res.status(200).json({ body })
                next()
            } catch {
                res.status(500).json({ error: 'Problem refreshing'})
            }
        }   
        
    }
}
const SpotifyApi = {
    getMe: async (req, res, next) => {

        let token = req.headers.authorization.split(' ').pop()

        if(token){
            let spotifyApi = new SpotifyWebApi({
                clientId: credentials.clientId,
                accessToken: token
            })
            try{

                let { body, headers, statusCode } = await spotifyApi.getMe()
                if(statusCode !== 200) throw new Error ('Bad request')
                res.status(200).json({ body })
                next()
            } catch (err) {
                res.status(500).json({error: 'Authorization unsuccesful'})
                throw new Error ('Internal error calling Spotify API')
            }
        } else {
            res.status(500).json({error: 'Authorization unsuccesful'})
        }
    },
    getUserPlaylists: async (req, res, next) => {

        let token = req.headers.authorization.split(' ').pop()
        let id = req.body.id

        if(token){
            let spotifyApi = new SpotifyWebApi({
                clientId: credentials.clientId,
                accessToken: token
            })
            try {
                let response = await spotifyApi.getUserPlaylists(id)
                res.status(200).json({ response })
                next()
            } catch (err) {
                res.json({ status: 'Problem', err })
                next()
            }
        } else {
            res.status(500).json({error: 'Authorization unsuccesful'})
        }
    },
}
module.exports = { SpotifyHandler, SpotifyApi }

   

