const express = require('express')
const router = express.Router()
const { nanoid }  = require('nanoid')
const { SpotifyHandler, SpotifyApi } = require('../services/spotifyHandler')




router.get('/login', SpotifyHandler.login, (req, res) => {   })

router.get('/callback', SpotifyHandler.callback, (req,res) => {
    console.log('Callback successful')
})

router.get('/refresh', SpotifyHandler.refresh_token, (req, res) => {   })

router.get('/spotify/me', SpotifyApi.getMe, (req, res) => {
    
})

router.get('/spotify/playlists', SpotifyApi.getUserPlaylists,(req, res) => {
    
})
router.delete('/destory_session', async (req, res) => {
    // TODO END SESSIONs
})

module.exports = router