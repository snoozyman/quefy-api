const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const session = require('express-session')
const { nanoid } = require('nanoid')
var cookieParser = require('cookie-parser');
/*** Other files */
const indexRoutes = require('./routes/')
const config  = require('./config')
const app = express()

/*** Use statements  */
app.use(cors())
app.use(morgan("combined"))
app.use(helmet())
app.use(express.json())
app.use(cookieParser())
app.use(session({
    secret: config.session.secret,
    resave: false,
    saveUninitialized: true,
    genid: function(req) {
        return nanoid(20) // use UUIDs for session IDs
      },
    cookie: {
        maxAge: 60000
    }
}))


/*** Routing */

app.use('/', indexRoutes)

// Errors

app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});



app.use((err, req, res) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	if (!process.env.NODE_ENV) {
		console.log(err);
	}
	res.status(err.status || 500).send({
		error: true,
		message: err.message,
		error_detail: {
			message: err.message,
			code: err.status,
		},
	});
});

module.exports = app
