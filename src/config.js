require('dotenv').config()

module.exports = {
    server: {
        baseUrl: 'http://localhost:8080',
        port: process.env.SERVER_PORT || 8080,
        clientId: '91769ff8707342f8a342fa2168c8ede0',
        clientSecret: process.env.CLIENT_SECRET
    },
    session: {
        secret: process.env.SESSION_SECRET || 'pinkunicorn',
    }
    
}