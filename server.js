const app = require('./src/app')
const config = require('./src/config')


app.listen(config.server.port, () => {
    console.log(`Server started at ${config.server.port}`)
});